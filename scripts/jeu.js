'use strict'

// Validation du formulaire
const formulaire = document.getElementById('formulaire')
const nom = document.getElementById('nom')
const paires = document.getElementById('number')
const btnSoumettre = document.getElementById('soumettre')
let erreur = []
const ul = document.getElementById('liste_erreur')
const nomJoueur = document.getElementById('nomJoueur')
const game_container = document.getElementById('game_container')
const nomValider = /[^a-z0-9' -]/i
const texteVictoire = document.getElementById('victoire')

const messagesErreur = {
    nom: "Veuillez entrer votre nom",
    nomAlpha: "Votre nom doit être alphanumérique",
    nbPaires: "Vous devez choisir une paire entre 2 et 10"
}

/**
 *Ajout evenement soumettre sur le bouton du formulaire
 *
 */
function ajoutEvenements(){
    formulaire.addEventListener('submit', valider)
}

/**
 *Lorsuq'on clic sur soumettre, execute la function Valider qui s'assure que certains champ sont vides et valide les informations du formulaire 
 * Crée le jeu si pas d'erreur
 *
 * @param {*} e
 */
function valider(e){
    game_container.innerHTML = ""
    texteVictoire.innerHTML = ""
    ul.innerHTML = ""
    divCompteur.innerHTML = ""
    erreur = []
    validerNom()
    validerPaires()
    if(erreur.length > 0) {
        ajouterErreur()
        e.preventDefault()
    } else {
        minuterie()
        jeuMemoire()
        e.preventDefault()
    }
}
    
/**
 *Valider si le nom est écrit et si dans un bon format
 * sinon ajoute erreur
 */
function validerNom(){
    const valeurNom = nom.value
    if (valeurNom.length <= 0){
        erreur.push(messagesErreur.nom)
        erreurAffichage(nom)
    } 
    if(nomValider.test(valeurNom)){
        erreur.push(messagesErreur.nomAlpha)
        erreurAffichage(nom)
    } else {
        erreurRemove(nom)
        nomJoueur.textContent = valeurNom
    }
}

/**
 * Valide si le bon nombre de paires est entrer
 *
 */
function validerPaires(){
    const valeurPaires = paires.value
    if (valeurPaires < 2 || valeurPaires > 10){
        erreur.push(messagesErreur.nbPaires)
        erreurAffichage(paires)
    } else{
        erreurRemove(paires)
    }
}

/**
 *Ajoute la classe erreur au champ 
 *
 * @param {*} champ
 */
function erreurAffichage(champ){
    champ.classList.add('input-erreur')
}

/**
 *Remove la classe erreur du champ
 *
 * @param {*} champ
 */
function erreurRemove(champ){
    champ.classList.remove('input-erreur')
}

/**
 * Ajouter les erreurs au document
 *
 */
function ajouterErreur(){
    for(let i = 0; i < erreur.length; i++){
        const li = document.createElement('li')
        const messageErreur = erreur[i]
        const texteErreur = document.createTextNode(messageErreur)
        li.appendChild(texteErreur)
        ul.appendChild(li)
    }

}

/** Le jeu
 * la function jeuMemeoire contient le jeu, elle est appeler losrque le formulaire est valider
 *Les cartes de jeux sont mélanger selon le nombre de paires inscrit par l'utilisateur
 *
 */
function jeuMemoire(){
    const nbPaires = paires.value
    const tableauCartes = []
  
    for(let i = 0; i < nbPaires; i++){ // ajoute le nombre de paires au tableau
        tableauCartes.push(i)
        tableauCartes.push(i)
    }
    console.log(tableauCartes)
    const tableauCartesMelangees = []
    while(tableauCartes.length > 0) { // melagner les cartes selon la longueur du tableau (paires)
        const index = Math.floor(Math.random() * tableauCartes.length)
        tableauCartesMelangees.push(tableauCartes[index])
        tableauCartes.splice(index, 1)
    }
    console.log(tableauCartesMelangees)
    for(let j = 0; j < tableauCartesMelangees.length; j++){  // pour chaque carte ajoute la classe bouton et le clic
        const boutonCarte = document.createElement('button')
        const numeroCarte = tableauCartesMelangees[j]
        boutonCarte.classList.add('bouton_jeu')
        boutonCarte.classList.add('bg-carte')
        boutonCarte.setAttribute('data-cachee', numeroCarte)
        const carteCachee = document.createTextNode('?')
        boutonCarte.appendChild(carteCachee)
        game_container.appendChild(boutonCarte)
        boutonCarte.addEventListener('click', tournerCarte)
    }
        let cartesTournees = []        
        let pairesTrouvees = 0
        /**
         *tourner la carte lors du clic et afficher le contenu
         *Si carte pareille +1 aux paires trouvées sinon cacher les cartes
         * @param {*} e
         */
        function tournerCarte(e){
            cartesTournees.push(e.target)
            console.log(cartesTournees)
            const numeroCarte = e.target.getAttribute('data-cachee')
            e.target.textContent = numeroCarte
            cartesTournees[0].setAttribute('disabled', true)
            cartesTournees[0].classList.remove('bg-carte')
            cartesTournees[0].classList.add('carteOuverte')
    
            if(cartesTournees.length === 2){ // si il y deux carte de cliquer
                let carte1 = cartesTournees[0]
                let carte2 = cartesTournees[1]
                carte2.setAttribute('disabled', true)
                carte2.classList.remove('bg-carte')
                carte2.classList.add('carteOuverte')

                if(carte1.textContent === carte2.textContent){ // et si la carte 1 = carte 2 les cartes 1 et 2 deviennent invalide et +1 aux paires trouvées
                    cartesTournees[0].setAttribute('disabled', true)
                    cartesTournees[1].setAttribute('disabled', true)
                    pairesTrouvees = pairesTrouvees + 1
                    cartesTournees = []
                    console.log(cartesTournees)
                    console.log(pairesTrouvees)
                } else { // sinon cacher les cartes apres 1 seconde
                    setTimeout(cacherCarte, 1000)
                    /**
                     *Apres 1 seconde cacher les cartes qui ont été cliqué
                     *
                     */
                    function cacherCarte(){
                        carte1.textContent = '?'
                        carte2.textContent = '?'
                        carte1.removeAttribute('disabled')
                        carte2.removeAttribute('disabled')
                        carte1.classList.add('bg-carte')
                        carte2.classList.add('bg-carte')
                    }
                    
                    cartesTournees = []
                }
                
            }
            if(pairesTrouvees == nbPaires){  //si les paires trouvées sont = nb de paires
                texteVictoire.textContent = 'Victoire! Vous avez trouvé toutes les cartes!'
                divCompteur.innerHTML = ''
                setTimeout(victoire, 500)
                /**
                 *Afficher victoire apres le timer lorsque toute les paires sont trouvéess
                 *
                 */
                function victoire(){
                    alert('Victoire!')
                }
            }
            
        }
}


// minuterie 
/**
 * Crée la minuterie pour le jeu
 *
 */
function minuterie(){
    const compteurStart = 5 // competeur commence a 5 pour 5 minutes
    let temps = compteurStart * 60 // signifie le temps (5 minutes) en secondes
    const divCompteur = document.getElementById('divCompteur')
    divCompteur.innerHTML = ''
    const compteur = document.createElement('div')
    divCompteur.appendChild(compteur)
    const texteTemps = document.getElementById('temps')
    texteTemps.classList.remove('hidden')


    let repete = setInterval(decompte, 1000) // l'interval de temps pour changer a chaque seconde le compteur 
    /**
     *decompte de 5 minute va s'affichier sur le html dans le paragraphe compteur
     *
     */
    function decompte() {
        const minutes = Math.floor(temps / 60)
        let secondes = temps % 60
        if(secondes < 10){  // pour faire afficher le 0 avant les secondes qui sont en dessous de 10
            secondes = '0' + secondes
        } else {
            secondes
        }
        compteur.innerHTML = `${minutes}: ${secondes}` // afficher les minutes et secondes dans le html
        temps-- // le temps en decroissance
        if ((minutes <= 0) && (secondes <= 0)) {
            clearInterval(repete)
            texteVictoire.textContent = 'Vous avez perdu, le temps est écoulé.'
            game_container.innerHTML = ''
        }
    }
}



ajoutEvenements()